import pytest


@pytest.mark.asyncio
async def test_get_all_routes(web_retriever_paths, web_retriever_client):
    for path in web_retriever_paths:
        resp = await web_retriever_client.get(path)
        # we're just validating that all routes are up, not that they work
        assert resp.status in [200, 400]


@pytest.mark.asyncio
async def test_invalid_route(web_retriever_client):
    resp = await web_retriever_client.get("/invalid_route")
    assert resp.status == 404
