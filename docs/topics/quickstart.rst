Quickstart
==========

Getting started with Web Retriever is straightforward. Follow the steps below to set up a simple scenario to familiarize
yourself with its functionalities.

Prerequisites
-------------

- Ensure that you have Python 3.8+ installed.
- Git should be installed if you plan to clone the repository.

Installation
------------

1. **Install from PyPI:**

   .. code-block:: bash

      pip install web-retriever

2. **Alternatively, install from source:**

   .. code-block:: bash

      git clone git@gitlab.com/hoprco/web-retriever.git
      cd web-retriever
      python3 -m venv .venv
      source .venv/bin/activate
      pip install .

Basic Usage
-----------

1. **Create a YAML configuration file with your rules.** Here is a simple example:

   .. code-block:: yaml

      web_retriever:
        rules:
          - rule_type: "allow"
            rule_string: "remote == '127.0.0.1'"

2. **Run Web Retriever with your configuration file:**

   .. code-block:: text

      $ web-retriever -c config.yaml

      ======== Running on http://0.0.0.0:8080 ========
      (Press CTRL+C to quit)

3. **Test the setup by making a request to the Web Retriever API.**

   You should see the content from the remote API in the response.

   .. code-block:: text

      $ curl -s "localhost:8080/api/v1/fetch?url=https://catfact.ninja/fact&type=json" | jq

      {
        "status": "success",
        "data": [
          {
            "url": "https://catfact.ninja/fact",
            "type": "json",
            "content": {
              "fact": "A cat’s hearing is better than a dog’s. And a cat can hear high-frequency sounds up to two octaves higher than a human.",
              "length": 119
            }
          }
        ],
        "timestamp": "2023-10-18T20:26:44.648416"
      }

Congratulations! You’ve just set up and tested a basic scenario with Web Retriever. Explore further by trying out
different rules and configurations to fully harness the capabilities of Web Retriever.
