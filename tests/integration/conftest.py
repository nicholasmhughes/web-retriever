from unittest.mock import patch

import pytest


@pytest.fixture(scope="session", name="hub")
def integration_hub(hub):
    for dyne in ["web_retriever"]:
        hub.pop.sub.add(dyne_name=dyne)

    with patch("sys.argv", ["web_retriever"]):
        hub.pop.config.load(["web_retriever"], cli="web_retriever")

    yield hub


async def _setup_hub(hub):
    # Set up the hub before each function here
    ...


async def _teardown_hub(hub):
    # Clean up the hub after each function here
    ...


@pytest.fixture(scope="function", autouse=True)
async def function_hub_wrapper(hub):
    await _setup_hub(hub)
    yield
    await _teardown_hub(hub)


@pytest.fixture
def loop(event_loop):
    return event_loop


@pytest.fixture(scope="function")
def web_retriever_server(hub):
    with patch("sys.argv", ["web_retriever"]):
        yield hub.web_retriever.init.cli()


@pytest.fixture
def web_retriever_routes(hub):
    hub.web_retriever.init.ROUTES = []
    hub.web_retriever.fetch.add_routes()
    return hub.web_retriever.init.ROUTES


@pytest.fixture
async def web_retriever_app(hub, loop, web_retriever_routes):
    return await hub.server.web.init(routes=web_retriever_routes)


@pytest.fixture
async def web_retriever_paths(web_retriever_app, loop):
    paths = list(
        {
            _rt.get_info()["path"]
            for _rt in web_retriever_app.router.routes()
            if _rt.get_info().get("path")
        }
    )
    return paths


@pytest.fixture
async def web_retriever_client(hub, aiohttp_client, web_retriever_app):
    return await aiohttp_client(web_retriever_app)
