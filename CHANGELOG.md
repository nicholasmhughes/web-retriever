The changelog format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

This project uses [Semantic Versioning](https://semver.org/) - MAJOR.MINOR.PATCH

# Changelog

# Web_Retriever 0.1.0 (2023-06-01)

### Added

- Initial release of basic fetch capability.
