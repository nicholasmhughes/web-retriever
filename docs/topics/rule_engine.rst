Web Retriever: Rule Engine Capability
=====================================

Web Retriever utilizes a powerful Rule Engine that employs the rule-engine
library, allowing for dynamic and flexible rule application. Users can configure
rules through a YAML configuration file, specifying a list of rule dictionaries
to manage and manipulate the aiohttp Request object attributes.

Configuration
-------------

Rules are defined in the YAML configuration file under the `web_retriever` key.
Each rule consists of a `rule_type` and a corresponding `rule_string` or
`transform` block, depending on the rule type.

Rule Types
----------

- **Deny**: Rules that prevent certain requests based on specified conditions.
- **Allow**: Rules that permit requests based on specified conditions.
- **Transform**: Rules that modify request or response headers.

Rule Syntax
-----------

- **Deny and Allow Rules**: Utilize the rule-engine syntax acting on attributes of the `aiohttp Request <https://docs.aiohttp.org/en/stable/web_reference.html>`_ object.
- **Transform Rules**: Used to add or remove headers in the request or response.

Example Configuration
----------------------

.. code-block:: yaml

   web_retriever:
     rules:
       - rule_type: "deny"
         rule_string: "message.method != 'POST'"
       - rule_type: "allow"
         rule_string: "remote == '127.0.0.1' or remote == '::1'"
       - rule_type: "transform"
         transform:
           pass_request_headers: true
           pass_response_headers: false
           request_headers:
             add:
               X-API-Token: "something"
             remove:
               - "User-Agent"
           response_headers:
             add:
               X-Web-Retriever: "true"

In this example:

- A deny rule is set to block any request that is not a POST method.
- An allow rule is set to permit requests from localhost.
- A transform rule is set to modify request and response headers, such as adding and removing specific headers.
- Any traffic which does not meet **ALL** allow/deny rules will be rejected.

For more details on the rule-engine syntax, you can refer to the `rule-engine
library <https://github.com/zeroSteiner/rule-engine>`_. For more information on
the aiohttp Request object attributes, visit the `aiohttp documentation
<https://docs.aiohttp.org/en/stable/web_reference.html>`_.
